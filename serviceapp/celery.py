from __future__ import absolute_import, unicode_literals
from celery import Celery
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", 'serviceapp.settings')
os.environ.setdefault("CELERY_API_KEY", "AIzaSyCnZYhVucxKfqrkme7tnV4ro_wkbBk5ABI")

app = Celery("serviceapp")

app.config_from_object('django.conf:settings', namespace='CELERY')

app.autodiscover_tasks()


@app.task(bind=True)
def debug_task(self):
    print("Request: {0!r}".format(self.request))
