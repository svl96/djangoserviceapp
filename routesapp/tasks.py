from __future__ import absolute_import, unicode_literals
from celery import shared_task

from .utils.fill_addresses_table import fill_addresses
from .utils.fill_distance import fill_dist
from .utils.fill_clients import fill_clients, fill_managers
from .utils import calc_manager_routes
from .utils import solomon_problem


@shared_task()
def get_addresses():
    fill_addresses()
    pass


@shared_task()
def get_routes():
    fill_dist()
    pass


@shared_task
def get_clients():
    fill_clients()
    pass


@shared_task
def get_managers():
    fill_managers()
    pass


@shared_task
def calc_routes():
    calc_manager_routes.run()


@shared_task
def solomon_routes():
    # solomon_problem.run()
    pass
