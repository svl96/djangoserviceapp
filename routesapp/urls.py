from django.urls import path

from . import views

urlpatterns = [
    path('', views.index, name="index"),
    path('addr', views.addr, name="addr"),
    path('routes', views.routes, name="routes"),
    path('delete', views.delete_dist, name="delete"),
    path('managers', views.managers, name="managers"),
    path('clients', views.clients, name="clients"),
    path("manage", views.manage_routes, name="algo"),
    path("solomon", views.dists, name="solomon"),
    path("manager_route", views.get_manager_route, name="get_manager_route")

]
