import json
import os
from datetime import datetime

import googlemaps
from googlemaps.exceptions import ApiError
from sqlite3 import Error
from ..models import Address, Distance


def parse_results(start_addrs, dest_addrs, results):
    routes = []

    for i in range(len(start_addrs)):
        for j in range(len(dest_addrs)):
            try:
                origin = start_addrs[i]
                dest = dest_addrs[j]

                info = results['rows'][i]['elements'][j]

                distance = info['distance']
                duration = info['duration']
                duration_in_traffic = info['duration_in_traffic']

                distance_info = Distance.objects.update_or_create(
                    start_address=origin,
                    end_address=dest,
                    defaults={
                        'duration_value': duration['value'],
                        'duration_text': duration['text'],
                        'duration_in_traffic_value': duration_in_traffic['value'],
                        'duration_in_traffic_text': duration_in_traffic['text'],
                        'distance_value': distance['value'],
                        'distance_text': distance['text']
                    }
                )
                routes.append(distance_info)
            except IndexError as e:
                print(e)

    return routes


def get_routes(gmaps):
    addresses = Address.objects.all()

    for start_addr in addresses:
        departure_time = datetime.now()
        start_coords = [json.loads(start_addr.location)]
        start_addrs = [start_addr]

        end_addrs = []
        end_coords = []
        for addr in addresses:
            end_addrs.append(addr)
            end_coords.append(json.loads(addr.location))

        try:
            print("send request")
            dist_result = gmaps.distance_matrix(start_coords, end_coords, mode="driving", departure_time=departure_time)
            parse_results(start_addrs, end_addrs, dist_result)
            print(start_addr.name)

        except ApiError as e:
            print(e)
        except Error as e:
            print(e)
        except googlemaps.exceptions.Timeout:
            print('timeout', start_addr.name)


def fill_dist():
    api_key = os.environ["CELERY_API_KEY"]
    gmaps = googlemaps.Client(api_key)
    routes = get_routes(gmaps)

    print('finish')


