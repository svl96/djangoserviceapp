from ..models import Address, Client, Manager
import random


def fill_clients():
    addresses = Address.objects.all()
    clients = []
    with open("B:\Diplom\clients.txt", "r") as f:
        clients = [line.strip() for line in f.readlines()]


    count = 0
    for address in addresses:
        phone = "+79"
        for i in range(9):
            phone += str(random.randint(0, 9))
        name = clients[count]
        Client.objects.create(
            name=name,
            address=address,
            schedule="{\"start\": \"10:00\", \"end\": \"17:00\"}",
            contact=phone
        )
        count += 1

    print("done")


def fill_managers():
    managers = []
    with open("B:\Diplom\managers.txt", "r") as f:
        managers = [line.strip() for line in f.readlines()]

    for i in range(1, 11):
        Manager.objects.create(
            name=managers[i - 1],
            schedule="{\"start\": \"9:00\", \"end\": \"17:00\"}",
            contact="manager{}@managermail.com".format(i)
        )
