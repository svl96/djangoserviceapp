from typing import List, Dict, Tuple

from ..geneticAlgorithm.enitities import BaseChromosome
from ..geneticAlgorithm.chromosome_impl import ChromosomeFixedManagerCount
from ..models import Distance, Client, Manager, Address, ManagerRoute
from ..geneticAlgorithm import enitities as gae
from ..geneticAlgorithm import cost_functions as gaf
from ..geneticAlgorithm import genetic_func_impl
from ..geneticAlgorithm.genetic_algorithm_impl import GeneticAlgorithm
import logging


def get_distance_matrix() -> Dict[int, Dict[int, float]]:
    distance_matrix: Dict[int, Dict[int, float]] = {}
    distances = Distance.objects.all()

    for distance_record in distances:
        start_id: int = distance_record.start_address.id
        end_id: int = distance_record.end_address.id

        value: float = distance_record.distance_value
        if start_id not in distance_matrix:
            distance_matrix[start_id] = {}

        distance_matrix[start_id][end_id] = value

    return distance_matrix


def get_time_matrix() -> Dict[int, Dict[int, float]]:
    time_matrix: Dict[int, Dict[int, float]] = {}
    distances = Distance.objects.all()

    for distance_record in distances:
        start_id: int = distance_record.start_address.id
        end_id: int = distance_record.end_address.id

        value: float = distance_record.duration_value
        if start_id not in time_matrix:
            time_matrix[start_id] = {}

        time_matrix[start_id][end_id] = value

    return time_matrix


def get_customers(depot_id: int) -> List[gae.Customer]:
    clients = Client.objects.all()
    customers: List[gae.Customer] = []

    for client in clients:
        address_id = client.address.id
        if address_id == depot_id:
            continue
        schedule = gae.Schedule(3600, 28800)
        customer = gae.Customer(address_id, schedule, demand=1, service_time=0, penalty=50000)
        customers.append(customer)

    return customers


def get_managers(depot_id: int):
    db_managers = Manager.objects.all()[:6]
    schedule = gae.Schedule(0, 28800)
    depot = gae.Customer(depot_id, schedule, demand=0, service_time=0, penalty=0)
    ga_managers: List[gae.Manager] = []
    capacity = 10

    for manager in db_managers:
        uid = manager.id
        schedule = schedule
        ga_manager = gae.Manager(uid, depot, schedule, capacity)
        ga_managers.append(ga_manager)

    return ga_managers


def run():
    depot_id = 61
    print("start customers")
    customers = get_customers(depot_id)
    print("start managers")
    managers = get_managers(depot_id)
    print("start dist_mtr")
    dist_matrix = get_distance_matrix()
    print("start time mtr")
    time_matrix = get_time_matrix()

    crossover_prob = 0.6
    mutation_prob = 0.4
    opt_probability = 0.4
    iter_count = 200
    population_size = 30
    life_duration = 10

    funcs = gaf.CostFunctions(dist_matrix, time_matrix)
    implement = genetic_func_impl\
        .GeneticFunctionsWithFixedManagers(customers=customers,
                                           managers=managers,
                                           cost_functions=funcs,
                                           crossover_probability=crossover_prob,
                                           mutation_probability=mutation_prob,
                                           optimization_probability=opt_probability
                                           )
    print("start genetic_algo")
    genetic_algorithm = GeneticAlgorithm(genetic_func=implement,
                                         iter_count=iter_count,
                                         life_duration=life_duration,
                                         population_size=population_size)

    result: Tuple[int, float, ChromosomeFixedManagerCount] = genetic_algorithm.run()

    logging.info((result[0], result[1]))

    for route in result[2].routes:
        total_demand = 0
        for cust in route.get_route_without_depot():
            total_demand += cust.demand

        logging.info("id: {}, cap: {}, cost: {}".format(route.manager.id, route.manager.capacity, total_demand))
        print()

    for route in result[2].routes:
        custs = ""
        for cust in route.get_route_with_depot():
            custs += str(cust.id) + ", "
        logging.info(custs)

    fill_manager_routes_table((result[1], result[2]))


def fill_manager_routes_table(result: Tuple[float, ChromosomeFixedManagerCount]):
    for route in result[1].routes:
        manager_id = route.manager.id
        manager = Manager.objects.get(id=manager_id)

        index = 0
        for customer in route.get_route_without_depot():
            client_id = customer.id
            client = Client.objects.get(address_id=client_id)
            client_order = index

            ManagerRoute.objects.create(client=client, manager=manager, client_order=client_order)
            index += 1
