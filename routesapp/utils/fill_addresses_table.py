import googlemaps
import os
import time
import json
from ..models import Address, Client


def get_addresses():
    print(os.environ)
    API_KEY = os.environ["CELERY_API_KEY"]

    gmaps = googlemaps.Client(key=API_KEY)
    name = "Магазин Екатеринбург"
    results_key = "results"
    next_page_key = "next_page_token"
    pls = gmaps.places(name)
    results = pls['results']
    next_page = pls["next_page_token"]
    while len(results) < 100:
        time.sleep(2)
        pls = gmaps.places(name, page_token=next_page)

        next_page_result = pls['results']
        results = results + next_page_result
        if len(next_page_result) < 20:
            break
        try:
            next_page = pls['next_page_token']
        except KeyError:
            break
    return results


def add_to_database(results):
    for result in results:
        formated_address = result["formatted_address"]
        location = json.dumps(result['geometry']['location'])
        addr = Address(name=formated_address, location=location)
        addr.save()


def fill_addresses():
    request_result = get_addresses()
    add_to_database(request_result)
    print("finish")
