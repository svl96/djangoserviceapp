import logging

from ..models import ManagerRoute, Client


def get_manager_route(manager_id=41):
    manager_routes = ManagerRoute.objects.filter(manager_id=manager_id)
    res = []
    for manager_route in manager_routes:
        client: Client = manager_route.client
        client_id = client.id
        client_name = client.name
        client_address = client.address.name
        client_phone = client.contact
        client_location = client.address.location

        data = {"uid": client_id,
                "name": client_name,
                "address": client_address,
                "phone": client_phone,
                "location": client_location}

        res.append(data)

    return res
