from typing import List, Dict

import numpy as np
import math

from ..geneticAlgorithm.genetic_algorithm_impl import GeneticAlgorithm
from ..geneticAlgorithm.genetic_func_impl import GeneticFunctionsWithFixedManagers
from ..geneticAlgorithm.cost_functions import CostFunctions
from ..geneticAlgorithm.enitities import Customer, Schedule, Manager


def calc_dist_matrix(depot, clients):
    dist_matrix: Dict[int, Dict[int, float]] = {}
    coords = [depot] + clients

    for i in range(len(coords)):
        dist_matrix[i] = {}
        for j in range(len(coords)):
            start_point = int(coords[i][1]), int(coords[i][2])
            end_point = int(coords[j][1]), int(coords[j][2])

            dist_val = math.sqrt(
                math.pow((start_point[0] - end_point[0]), 2) + math.pow((start_point[1] - end_point[1]), 2))

            dist_matrix[i][j] = dist_val

    return dist_matrix


def read_from_file():
    depot = []

    clients = []

    with open("B:\Diplom\data\c101.txt", "r") as f:
        lines = list(filter(lambda val: val != "\n", f.readlines()))
        name = lines[0]
        num_vehicle, capacity = list(filter(lambda val: val != '', lines[3].strip().split(" ")))

        print(num_vehicle, capacity)
        depot = list(filter(lambda val: val != '', lines[7].strip().split(" ")))

        for i in range(1, 101):
            client = list(filter(lambda val: val != '', lines[7 + i].strip().split(" ")))
            clients.append(client)

    return int(num_vehicle), int(capacity), depot, clients


def get_customers(clients):
    customers: List[Customer] = []

    for client in clients:
        client_id = int(client[0])
        demand = int(client[3])
        start = int(client[4])
        end = int(client[5])
        schedule = Schedule(start=start, end=end)
        penalty = int(client[6])
        service_time = int(client[6])

        customer = Customer(client_id, schedule, penalty, demand, service_time)

        customers.append(customer)

    return customers


def get_managers(manger_count: int, capacity: int, depot: Customer, start, end):
    managers: List[Manager] = []
    max_count = 30
    max_cost = capacity

    for i in range(manger_count):
        id = i
        schedule = Schedule(start=start, end=end)
        manager = Manager(id, depot, schedule, max_count, capacity)
        managers.append(manager)

    return managers


def get_depot(depot) -> Customer:
    depot_id = int(depot[0])
    demand = int(depot[3])
    start = int(depot[4])
    end = int(depot[5])
    schedule = Schedule(start=start, end=end)
    penalty = int(depot[6])
    service_time = int(depot[6])

    out_depot = Customer(depot_id, schedule, penalty, demand, service_time)

    return out_depot


def run_genetic_algo(managers_count, capacity, depot_rec, clients):
    customers: List[Customer] = get_customers(clients)
    start = int(depot_rec[4])
    end = int(depot_rec[5])
    depot: Customer = get_depot(depot_rec)

    crossover_prob = 0.6
    mutation_prob = 0.0
    iter_count = 500
    population_size = 50

    managers: List[Manager] = get_managers(managers_count, capacity, depot, start, end)

    dist_matrix = calc_dist_matrix(depot_rec, clients)
    time_matrix = dist_matrix

    funcs = CostFunctions(dist_matrix, time_matrix)
    implement = GeneticFunctionsWithFixedManagers(
        customers,
        managers,
        funcs,
        crossover_prob,
        mutation_prob
    )

    genetic_algo = GeneticAlgorithm(
        genetic_func=implement,
        iter_count=iter_count,
        population_size=population_size,
        life_duration=100
    )

    result = genetic_algo.run()

    best = min(result, key=lambda v: v[0])
    print(best[0], str(best[1]))

    best_chrom = best[1]

    for route in best_chrom.routes:
        total_demand = 0
        for cust in route.get_route_without_depot():
            total_demand += cust.demand
        print("id: {}, cap: {}, cost: {}".format(route.manager.id, route.manager.max_cost, total_demand))

def run():
    num_vehicle, capacity, depot, clients = read_from_file()
    run_genetic_algo(20, capacity, depot, clients)

