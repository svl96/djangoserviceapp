from django.db import models

# Create your models here.


class Address(models.Model):
    class Meta:
        db_table = "addresses"
    name = models.CharField(max_length=200)
    location = models.TextField()


class Client(models.Model):
    class Meta:
        db_table = "clients"
    name = models.CharField(max_length=200)
    schedule = models.TextField()
    address = models.ForeignKey(Address, on_delete=models.CASCADE)
    contact = models.CharField(max_length=40)


class Manager(models.Model):
    class Meta:
        db_table = "managers"

    name = models.CharField(max_length=200)
    schedule = models.TextField()
    contact = models.CharField(max_length=40)


class Distance(models.Model):
    class Meta:
        db_table = "distances"
        unique_together = (("start_address", "end_address"), )

    start_address = models.ForeignKey(Address, related_name="start_address", on_delete=models.CASCADE)
    end_address = models.ForeignKey(Address, related_name="end_address", on_delete=models.CASCADE)

    duration_value = models.IntegerField()
    duration_text = models.CharField(max_length=100)

    duration_in_traffic_value = models.IntegerField()
    duration_in_traffic_text = models.CharField(max_length=100)

    distance_value = models.IntegerField()
    distance_text = models.CharField(max_length=100)


class ManagerRoute(models.Model):
    class Meta:
        db_table = "routes"

    manager = models.ForeignKey(Manager, on_delete=models.CASCADE)
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    client_order = models.IntegerField()





