from django.core.management.base import BaseCommand, CommandError

from ...models import Manager


class Command(BaseCommand):

    def handle(self, *args, **options):
        for i in range(20):
            manager_name = "manager{}".format(i)
            schedule = '{\"start\": \"10:00\", \"end\": \"17:00\" }'
            contact = manager_name + "@test.test"
            manager = Manager(name=manager_name, schedule=schedule, contact=contact)

            manager.save()

        print("success")
