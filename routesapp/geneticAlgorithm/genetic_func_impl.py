from .chromosome_impl import ChromosomeFixedManagerCount
from .cost_functions import CostFunctions
from .enitities import GeneticFunctions, Customer, Manager, Route
from typing import List, Dict, Tuple, Set

import numpy as np
import numpy.random as rnd


class GeneticFunctionsWithFixedManagers(GeneticFunctions):
    def __init__(self, customers: List[Customer],
                 managers: List[Manager],
                 cost_functions: CostFunctions,
                 crossover_probability: float,
                 mutation_probability: float,
                 optimization_probability: float):

        self._crossover_probability = crossover_probability
        self._mutation_probability = mutation_probability
        self._optimization_probability = optimization_probability

        self._cost_functions: CostFunctions = cost_functions
        self._customers: List[Customer] = customers
        self._managers: List[Manager] = managers

    @property
    def crossover_probability(self) -> float:
        return self._crossover_probability

    @property
    def mutation_probability(self) -> float:
        return self._mutation_probability

    @property
    def optimization_probability(self) -> float:
        return self._optimization_probability

    def fitness(self, chromosome: ChromosomeFixedManagerCount) -> float:
        total_cost = 0
        routes: List[Route] = chromosome.routes
        unvisited: List[Customer] = chromosome.unvisited_customers

        for route in routes:
            total_cost += self._cost_functions.get_dist_cost(route.get_route_with_depot())

        for customer in unvisited:
            total_cost += customer.penalty

        return total_cost

    def parents(self, fits_population: List[Tuple[float, ChromosomeFixedManagerCount]]):
        population_size = len(fits_population)

        index1, index2 = rnd.choice(population_size, 2, False)
        parent1: ChromosomeFixedManagerCount = fits_population[index1][1]
        parent2: ChromosomeFixedManagerCount = fits_population[index2][1]

        return parent1, parent2

    def create_chromosome(self) -> ChromosomeFixedManagerCount:
        unvisited_indexes = {i for i in range(len(self._customers))}
        manager_routes: Dict[int, List[Customer]] = {}

        for manager in self._managers:
            current_route = []

            if len(unvisited_indexes) > 0:
                seed_index: int = rnd.choice(list(unvisited_indexes), 1)[0]
                unvisited_indexes.discard(seed_index)

                current_route: List[Customer] = [manager.depot, self._customers[seed_index]]
                if len(unvisited_indexes) == 0:
                    manager_routes[manager.id] = current_route + [manager.depot]

            while len(unvisited_indexes) > 0:
                next_customer_index: int = self._cost_functions\
                    .get_closest_customer_index(current_route, self._customers,
                                                unvisited_indexes, manager)

                if next_customer_index >= 0:
                    customer: Customer = self._customers[next_customer_index]
                    current_route.append(customer)
                    unvisited_indexes.discard(next_customer_index)
                else:
                    manager_routes[manager.id] = current_route + [manager.depot]
                    break

                if len(unvisited_indexes) == 0:
                    manager_routes[manager.id] = current_route + [manager.depot]

        unvisited_customers: List[Customer] = []

        for i in unvisited_indexes:
            unvisited_customers.append(self._customers[i])

        chromosome = self.init_chromosome(manager_routes, unvisited_customers)

        is_valid = self._cost_functions.check_validity(chromosome.routes,
                                                       chromosome.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Init Invalid")

        return chromosome

    def init_chromosome(self, manager_routes: Dict[int, List[Customer]],
                        unvisited: List[Customer]) -> ChromosomeFixedManagerCount:
        chromosome: ChromosomeFixedManagerCount = ChromosomeFixedManagerCount()

        for manager in self._managers:
            if manager.id in manager_routes:
                route = Route(manager)

                for customer in manager_routes[manager.id]:
                    route.add_customer(customer)

                chromosome.append_route(route)

        for customer in unvisited:
            chromosome.append_unvisited_customer(customer)

        return chromosome

    def init(self, population_size: int) -> List[ChromosomeFixedManagerCount]:
        return [self.create_chromosome() for _ in range(population_size)]

    def get_filtered_manager_routes(self, routes: List[Route],
                                    unvisited: List[Customer],
                                    rnd_clients_ids: Set[int]) -> Tuple[Dict[int, List[Customer]],
                                                                        List[Customer]]:
        manager_routes: Dict[int, List[Customer]] = {}
        out_unvisited: List[Customer] = []

        for route in routes:
            manager: Manager = route.manager

            if manager.id not in manager_routes:
                manager_routes[manager.id] = []

            customers: List[Customer] = route.get_route_without_depot()
            for customer in customers:
                if customer.id not in rnd_clients_ids:
                    new_route: List[Customer] = manager_routes[manager.id]
                    new_route.append(customer)

        for customer in unvisited:
            if customer.id not in rnd_clients_ids:
                out_unvisited.append(customer)

        return manager_routes, out_unvisited

    def insert_customer_best_fit(self, manager_routes: Dict[int, List[Customer]],
                                 unvisited: List[Customer], clint_ids: Set[int]):

        for customer in self._customers:
            if customer.id not in clint_ids:
                continue

            best_pos = -1
            best_manager_id = -1
            best_append_cost = float('inf')

            for manager in self._managers:
                if manager.id not in manager_routes:
                    continue

                list_customer: List[Customer] = manager_routes[manager.id]

                pos, cost, time = self._cost_functions.get_best_append_position(
                    list_customer, customer, manager)

                if pos >= 0 and cost < best_append_cost:
                    best_pos = pos
                    best_manager_id = manager.id
                    best_append_cost = cost

                    if rnd.sample() < 0.1:
                        break

            if best_pos < 0:
                appended = False
                for manager in self._managers:
                    if manager.id not in manager_routes:
                        manager_routes[manager.id] = [customer]
                        appended = True
                        break
                if not appended:
                    unvisited.append(customer)
            else:
                route: List[Customer] = manager_routes[best_manager_id]

                route.insert(best_pos, customer)
                time = self._cost_functions.get_time_cost([self._managers[0].depot] + route + [self._managers[0].depot])
                if time > self._managers[0].schedule.end:
                    print("after")

    def crossover(self, parents) -> Tuple[ChromosomeFixedManagerCount, ChromosomeFixedManagerCount]:

        chromosome1: ChromosomeFixedManagerCount = parents[0]
        chromosome2: ChromosomeFixedManagerCount = parents[1]

        is_valid = self._cost_functions.check_validity(chromosome1.routes, chromosome1.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Before Crossover invalid")

        is_valid = self._cost_functions.check_validity(chromosome2.routes, chromosome2.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Before Crossover invalid")

        routes1: List[List[Customer]] = [r.get_route_without_depot() for r in chromosome1.routes
                                         if len(r.get_route_without_depot()) > 0]
        if len(chromosome1.unvisited_customers) > 0:
            routes1.append(chromosome1.unvisited_customers)

        routes2: List[List[Customer]] = [r.get_route_without_depot() for r in chromosome2.routes
                                         if len(r.get_route_without_depot()) > 0]

        if len(chromosome2.unvisited_customers) > 0:
            routes2.append(chromosome2.unvisited_customers)

        index1 = rnd.choice(len(routes1))
        index2 = rnd.choice(len(routes2))

        rand_client_ids_route1 = {customer.id for customer in routes1[index1]}
        rand_client_ids_route2 = {customer.id for customer in routes2[index2]}

        filtered1: Tuple[Dict[int, List[Customer]], List[Customer]] = self.get_filtered_manager_routes(
            chromosome1.routes, chromosome1.unvisited_customers, rand_client_ids_route2
        )
        manager_routes1: Dict[int, List[Customer]] = filtered1[0]
        unvisited1: List[Customer] = filtered1[1]

        filtered2: Tuple[Dict[int, List[Customer]], List[Customer]] = self.get_filtered_manager_routes(
            chromosome2.routes, chromosome2.unvisited_customers, rand_client_ids_route1
        )
        manager_routes2: Dict[int, List[Customer]] = filtered2[0]
        unvisited2: List[Customer] = filtered2[1]

        self.insert_customer_best_fit(manager_routes1, unvisited1, rand_client_ids_route2)
        self.insert_customer_best_fit(manager_routes2, unvisited2, rand_client_ids_route1)

        offspring1 = self.init_chromosome(manager_routes1, unvisited1)
        offspring2 = self.init_chromosome(manager_routes2, unvisited2)

        is_valid = self._cost_functions.check_validity(offspring1.routes, offspring1.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Crossover invalid1")

        is_valid = self._cost_functions.check_validity(offspring2.routes, offspring2.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Crossover invalid2")

        return offspring1, offspring2

    def mutation(self, chromosome: ChromosomeFixedManagerCount) -> ChromosomeFixedManagerCount:
        # recreate

        customers: List[Customer] = []
        manager_count = 0
        for route in chromosome.routes:
            if len(route.get_route_without_depot()) > 0:
                manager_count += 1

            for customer in route.get_route_without_depot():
                customers.append(customer)

        for customer in chromosome.unvisited_customers:
            customers.append(customer)

        current_customer_index: int = 0
        manager_routes: Dict[int, List[Customer]] = {}

        for manager in self._managers:
            current_route : List[Customer] = []
            while current_customer_index < len(customers):

                customer: Customer = customers[current_customer_index]
                is_acceptable, dist = self._cost_functions.is_acceptable_route(manager,
                                                                               current_route + [customer])

                if is_acceptable:
                    current_route.append(customer)
                    current_customer_index += 1
                else:
                    current_route: List[Customer] = [manager.depot] + current_route + [manager.depot]
                    manager_routes[manager.id] = current_route
                    current_route = []
                    break

            if current_customer_index >= len(customers) and len(current_route) > 0:
                current_route: List[Customer] = [manager.depot] + current_route + [manager.depot]
                manager_routes[manager.id] = current_route

        unvisited: List[Customer] = []
        while current_customer_index < len(customers):
            unvisited.append(customers[current_customer_index])
            current_customer_index += 1

        new_chromosome = self.init_chromosome(manager_routes, unvisited)

        is_valid = self._cost_functions.check_validity(new_chromosome.routes,
                                                       new_chromosome.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Mutation invalid")

        return new_chromosome

    def swap2opt(self, route: List[Customer], i, k) -> List[Customer]:
        return route[:i] + route[i:k][::-1] + route[k:]

    def algo2opt(self, manager: Manager, route: List[Customer]) -> Tuple[List[Customer], float]:
        min_distance = self._cost_functions.get_dist_cost(route)
        min_route = route
        size = len(route)

        for i in range(1, size - 2):
            for j in range(i + 2, size - 1):
                new_route = self.swap2opt(min_route, i, j)
                is_acceptable, dist = self._cost_functions.is_acceptable_route(manager, new_route)

                if is_acceptable and dist < min_distance:
                    min_distance = dist
                    min_route = new_route

        return min_route, min_distance

    def optimize(self, chromosome):

        is_valid = self._cost_functions.check_validity(chromosome.routes, chromosome.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("Before Optimize invalid")

        managers: Dict[int, Manager] = {manager.id: manager for manager in self._managers}
        manager_routes: Dict[int, List[Customer]] = \
            {r.manager.id: r.get_route_with_depot() for r in chromosome.routes}

        for manager_id in manager_routes.keys():
            route = manager_routes[manager_id]
            if len(route) > 2:
                new_route, cost = self.algo2opt(managers[manager_id], manager_routes[manager_id])
                manager_routes[manager_id] = new_route

        out_chromosome = self.init_chromosome(manager_routes, chromosome.unvisited_customers)

        is_valid = self._cost_functions.check_validity(out_chromosome.routes, out_chromosome.unvisited_customers,
                                                       self._customers)
        if not is_valid:
            print("After optimize invalid")

        return out_chromosome
