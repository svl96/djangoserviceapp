from typing import Dict, List, Set, Tuple

from .enitities import Customer, Manager, Schedule, Route


class CostFunctions:
    def __init__(self, dist_matrix: Dict[int, Dict[int, float]],
                 time_matrix: Dict[int, Dict[int, float]]):
        self._dist_matrix = dist_matrix
        self._time_matrix = time_matrix

    def get_dist_cost(self, route: List[Customer]) -> float:
        elements_count = len(route)

        total_dist = 0
        for i in range(elements_count - 1):
            start_point = route[i]
            end_point = route[i + 1]

            total_dist += self._dist_matrix[start_point.id][end_point.id]

        return total_dist

    def get_time_cost(self, route: List[Customer]) -> float:
        elements_count = len(route)

        total_time = 0
        for i in range(elements_count - 1):
            start_customer = route[i]
            end_customer = route[i + 1]

            travel_time = self._time_matrix[start_customer.id][end_customer.id]

            start_time = end_customer.schedule.start
            end_time = end_customer.schedule.end
            service_time = end_customer.service_time

            if total_time + travel_time > end_time:
                return float('inf')

            if total_time + travel_time < start_time:
                total_time = start_time + service_time
            else:
                total_time = total_time + travel_time + service_time

        return total_time

    def get_total_demand(self, route: List[Customer]) -> float:
        total_demand = 0

        for customer in route:
            total_demand += customer.demand

        return total_demand

    def get_closest_customer_index(self, route: List[Customer],
                                   all_customers: List[Customer],
                                   unvisited_index: Set[int],
                                   manager: Manager) -> int:

        min_dist = float("inf")
        min_index = -1

        current_demand = self.get_total_demand(route)
        current_time = self.get_time_cost(route)
        last_customer = route[-1]

        for index in unvisited_index:
            customer = all_customers[index]
            dist_from_last_to_new = self._dist_matrix[last_customer.id][customer.id]
            time_from_last_to_new = self._time_matrix[last_customer.id][customer.id]
            time_from_new_to_depot = self._time_matrix[customer.id][manager.depot.id]

            if current_time + time_from_last_to_new > customer.schedule.end:
                continue

            new_time = current_time + time_from_last_to_new + customer.service_time + \
                       time_from_new_to_depot

            if new_time > manager.schedule.end:
                continue

            if current_demand + customer.demand > manager.capacity:
                continue

            if dist_from_last_to_new < min_dist:
                min_dist = dist_from_last_to_new
                min_index = index
        #           if rnd.sample() < 0.01:
        #               break

        return min_index

    def check_validity(self, routes: List[Route], unvisited_cust: List[Customer],
                       all_customers: List[Customer]) -> bool:

        set_of_all = {cust.id for cust in all_customers}

        for route in routes:
            manager = route.manager
            total_time = self.get_time_cost(route.get_route_with_depot())

            if total_time > manager.schedule.end:
                return False

            total_demand = self.get_total_demand(route.get_route_with_depot())
            if total_demand > manager.capacity:
                return False

        for route in routes:
            for cust in route.get_route_without_depot():
                if cust.id in set_of_all:
                    set_of_all.discard(cust.id)
                else:
                    return False

        for cust in unvisited_cust:
            if cust.id in set_of_all:
                set_of_all.discard(cust.id)
            else:
                return False

        if len(set_of_all) > 0:
            return False

        return True

    def get_time_for_two(self, start: Customer, end: Customer) -> float:
        return self._time_matrix[start.id][end.id]

    def get_dist_for_two(self, start: Customer, end: Customer) -> float:
        return self._dist_matrix[start.id][end.id]

    def get_dist_cost_with_position(self, route: List[Customer],
                                    new_customer: Customer, position: int) -> float:

        new_route = route[:position] + [new_customer] + route[position:]
        return self.get_dist_cost(new_route)

    def get_time_cost_with_position(self, route: List[Customer],
                                    new_customer: Customer, position: int) -> float:

        new_route = route[:position] + [new_customer] + route[position:]
        return self.get_time_cost(new_route)

    def get_best_append_position(self, route: List[Customer],
                                 customer: Customer,
                                 manager: Manager) -> Tuple[int, float, float]:

        current_route = route
        if len(route) > 0:
            if route[0].id != manager.depot.id:
                current_route = [manager.depot] + current_route

            if route[-1].id != manager.depot.id:
                current_route = current_route + [manager.depot]

        else:
            current_route = [manager.depot, manager.depot]

        min_dist = float('inf')
        min_position = -1
        min_time = float('inf')
        current_demand = self.get_total_demand(current_route)

        for i in range(1, len(current_route)):

            new_time = self.get_time_cost_with_position(current_route, customer, i)
            new_dist = self.get_dist_cost_with_position(current_route, customer, i)
            new_demand = current_demand + customer.demand

            if new_demand > manager.capacity:
                continue

            if new_time > manager.schedule.end:
                continue

            if new_dist < min_dist:
                # минимальная позиция без учета депо
                min_position = i - 1
                min_dist = new_dist
                min_time = new_time
                # if rnd.sample() < 0.01:
                #     break

        return min_position, min_dist, min_time

    def is_acceptable_route(self, manager: Manager, route: List[Customer]) -> Tuple[bool, float]:

        current_route = route
        if len(route) > 0:
            if current_route[0].id != manager.depot.id:
                current_route = [manager.depot] + current_route

            if current_route[-1].id != manager.depot.id:
                current_route = current_route + [manager.depot]
        else:
            current_route = [manager.depot, manager.depot]

        demand = self.get_total_demand(current_route)
        time = self.get_time_cost(current_route)
        dist = self.get_dist_cost(current_route)

        if demand > manager.capacity:
            return False, dist

        if time > manager.schedule.end:
            return False, dist

        return True, dist
