from typing import List


class GeneticFunctions:

    @property
    def crossover_probability(self) -> float:
        return 0.0

    @property
    def mutation_probability(self) -> float:
        return 0.0

    @property
    def optimization_probability(self) -> float:
        return 0.0

    def init(self, population_size: int):
        return []

    def fitness(self, chromosome) -> float:
        return 0

    def parents(self, fits_population):
        return []

    def crossover(self, parents):
        return parents

    def mutation(self, chromosome):
        return chromosome

    def optimize(self, chromosome):
        return chromosome


class Schedule:
    def __init__(self, start, end):
        self._start = start
        self._end = end

    @property
    def start(self):
        return self._start

    @property
    def end(self):
        return self._end


class Customer:
    def __init__(self, uid, schedule: Schedule, penalty, demand: int=0, service_time: int=0):
        self._id = uid
        self._schedule = schedule
        self._penalty = penalty
        self._demand = demand
        self._service_time = service_time

    @property
    def id(self):
        return self._id

    @property
    def schedule(self):
        return self._schedule

    @property
    def penalty(self):
        return self._penalty

    @property
    def demand(self):
        return self._demand

    @property
    def service_time(self):
        return self._service_time

    def __str__(self):
        return "Customer:"+str(self._id)


class Manager:
    def __init__(self, uid, depot: Customer, schedule: Schedule, capacity):
        self._id = uid
        self._depot = depot
        self._schedule = schedule
        self._capacity = capacity

    @property
    def id(self):
        return self._id

    @property
    def depot(self) -> Customer:
        return self._depot

    @property
    def schedule(self) -> Schedule:
        return self._schedule

    @property
    def capacity(self):
        return self._capacity

    def __str__(self):
        return "Manager:" + str(self._id)


class Route:
    def __init__(self, manager: Manager):
        self._manager = manager
        self._customers: List[Customer] = []

    @property
    def depot(self) -> Customer:
        return self._manager.depot

    @property
    def manager(self) -> Manager:
        return self._manager

    def get_route_with_depot(self) -> List[Customer]:
        out_list: List[Customer] = [self.depot]
        for el in self._customers:
            out_list.append(el)
        out_list.append(self._manager.depot)
        return out_list

    def get_route_without_depot(self) -> List[Customer]:
        out_list = []
        for el in self._customers:
            out_list.append(el)

        return out_list

    def add_customer(self, customer: Customer):
        if customer.id != self.depot.id:
            self._customers.append(customer)

    def __str__(self):
        return "Route: (" + str(self._manager) + ", [" + ";".join(map(str, self._customers)) + "])"


class BaseChromosome:
    def __init__(self, parents=None):
        self._parents = parents
        self._lifespan: int = 0

    def increase_lifespan(self):
        self._lifespan += 1

    @property
    def lifespan(self):
        return self._lifespan

    def append_route(self, route: Route):
        pass

