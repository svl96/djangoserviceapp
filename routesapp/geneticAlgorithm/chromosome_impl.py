from typing import List

from .enitities import BaseChromosome, Route, Customer


class ChromosomeFixedManagerCount(BaseChromosome):
    def __init__(self, parents=None):
        super().__init__(parents)
        self._routes: List[Route] = []
        self._unvisited_customers: List[Customer] = []

    def append_route(self, route: Route):
        self._routes.append(route)

    def append_unvisited_customer(self, customer: Customer):
        self._unvisited_customers.append(customer)

    @property
    def routes(self):
        return self._routes

    @property
    def unvisited_customers(self):
        return self._unvisited_customers

    def __str__(self):
        return "[" + ";".join(map(str, self.routes)) + ";".join(map(str, self._unvisited_customers)) + "]"

