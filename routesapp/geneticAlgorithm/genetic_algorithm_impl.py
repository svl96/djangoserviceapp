from typing import List, Tuple, Iterable, Dict

from .enitities import GeneticFunctions, BaseChromosome

import numpy.random as rnd
import logging

class GeneticAlgorithm:
    def __init__(self, genetic_func: GeneticFunctions,
                 population_size: int, iter_count: int, life_duration: int):
        self.genetic_func: GeneticFunctions = genetic_func
        self.population_size: int = population_size
        self.iter_count: int = iter_count
        self.life_duration = life_duration

    def run(self):
        logging.basicConfig(filename="sample.log", level=logging.info)
        log = logging.getLogger("ex")
        logging.info("start algorithm")
        logging.info("population size")
        results_values: Dict[float, int] = {}

        population: List[BaseChromosome] = self.genetic_func.init(self.population_size)
        logging.info("after init population")
        for chromosome in population:
            chromosome.increase_lifespan()

        fits_population: List[Tuple[float, BaseChromosome]] = \
            [(self.genetic_func.fitness(chromosome), chromosome) for chromosome in population]

        for val in fits_population:
            value = round(val[0], 3)
            if value not in results_values:
                results_values[value] = 0
            results_values[value] += 1

        best_value = self.get_best_value(fits_population)
        best_result: Tuple[int, float, BaseChromosome] = (0, best_value[0], best_value[1])

        logging.info((best_result[0], best_result[1]))

        i = 0
        logging.info("start iteration")
        while i < self.iter_count:
            new_population = self.next(fits_population)

            generated_population = self.genetic_func.init(self.population_size)
            candidate_population = new_population + generated_population + population
            candidate_fits = [(self.genetic_func.fitness(chromosome), chromosome)
                              for chromosome in candidate_population]

            fits_population = self.select_next_population(candidate_fits, results_values)

            for val in fits_population:
                value = round(val[0], 3)
                if value not in results_values:
                    results_values[value] = 0
                results_values[value] += 1

            population: List[BaseChromosome] = [val[1] for val in fits_population]
            for el in population:
                el.increase_lifespan()
            best_value = self.get_best_value(fits_population)
            if best_value[0] < best_result[1]:
                best_result = (i, best_value[0], best_value[1])

            i += 1
            logging.info((i, best_value[0], self.get_average(fits_population)))
            # if i % 10 == 0:
            #     print(i, best_value[0])

        return best_result

    def get_average(self, fits_population: List[Tuple[float, BaseChromosome]]) -> float:
        total_value = 0
        for fit in fits_population:
            total_value += fit[0]

        return total_value / len(fits_population)

    def select_next_population(self, fits_population: List[Tuple[float, BaseChromosome]], results_values)\
            -> List[Tuple[float, BaseChromosome]]:

        out_population: List[Tuple[float, BaseChromosome]] = []
        out_values = set()
        for element in sorted(fits_population, key=lambda val:val[0]):
            chromosome = element[1]
            value = round(element[0], 3)
            if value in out_values:
                continue
            if (value in results_values and results_values[value] < self.life_duration) or (value not in results_values):
                if chromosome.lifespan < self.life_duration:
                    out_values.add(value)
                    out_population.append(element)
        out_population = self.get_sorted(out_population)

        return out_population[:self.population_size]

    def get_sorted(self, fits_population: List[Tuple[float, BaseChromosome]]) -> List[Tuple[float, BaseChromosome]]:
        return sorted(fits_population, key=lambda val: val[0])

    def get_best_value(self, fits_population: List[Tuple[float, BaseChromosome]]) -> Tuple[float, BaseChromosome]:
        best = min(fits_population, key=lambda val: val[0])
        return best

    def next(self, fits_popualtion) -> List[BaseChromosome]:
        new_population: List[BaseChromosome] = []

        while len(new_population) < self.population_size:
            parents = self.genetic_func.parents(fits_popualtion)

            cross = rnd.sample() < self.genetic_func.crossover_probability
            offsprings: Iterable[BaseChromosome] = self.genetic_func.crossover(parents) if cross else parents

            for offspring in offsprings:
                mutate = rnd.sample() < self.genetic_func.mutation_probability

                mutated_offspring = self.genetic_func.mutation(offspring) if mutate else offspring

                new_population.append(mutated_offspring)

        out_population: List[BaseChromosome] = []
        for el in new_population:
            opti = rnd.sample() < self.genetic_func.optimization_probability
            optimized = self.genetic_func.optimize(el) if opti else el
            out_population.append(optimized)

        return out_population
