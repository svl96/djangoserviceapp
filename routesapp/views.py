import time
from django.shortcuts import render
from django.http import JsonResponse
# Create your views here.
from .models import Distance
from .tasks import get_addresses, get_routes, get_managers, get_clients, calc_routes, solomon_routes
from .utils import manager_route_handler

def index(request):
    return JsonResponse({"test": "test"})


def addr(request):
    get_addresses.delay()
    return JsonResponse({"200": 'ok'})


def routes(request):
    get_routes.delay()
    return JsonResponse({"200": 'ok'})


def delete_dist(request):
    Distance.objects.all().delete()
    return JsonResponse({"200": 'ok'})


def clients(request):
    get_clients.delay()
    return JsonResponse({"200": 'ok'})


def managers(request):
    get_managers.delay()
    return JsonResponse({"200": 'ok'})


def manage_routes(request):
    calc_routes.delay()
    return JsonResponse({"200": 'ok'})


def dists(request):
    solomon_routes.delay()
    return JsonResponse({"200": 'ok'})


def get_manager_route(request):
    data = manager_route_handler.get_manager_route(41)
    return JsonResponse(data, safe=False)

